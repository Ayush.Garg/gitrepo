//
//  SecondViewController.swift
//  FirstApp
//
//  Created by cl-macmini-01 on 17/01/19.
//  Copyright © 2019 cl-macmini-01. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    var secondViewTitle: String = ""
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var label: UILabel!
    
    @IBAction func buttonAction(_ sender: Any) {
        if let controller =  self.storyboard?.instantiateViewController(withIdentifier: "NextViewController") as? NextViewController {
            
            controller.dismissCallback = { (value) -> Void in
                print("-----> \(value)")
                
            }
            
            
            controller.action2Callback = { (value) -> Void in
                
                print("-----> \(value)")

            }
            controller.delegate = self
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button.setTitle("Open Next View", for: .normal)
        
        self.title = "Second View Controller"
        // Do any additional setup after loading the view.
        label.text = secondViewTitle
        
        let leftBarItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(leftBarItemClicked))
        self.navigationItem.leftBarButtonItem = leftBarItem
        
    }
    
    @objc func leftBarItemClicked() {
        self.dismiss(animated: true) {
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SecondViewController: NextViewDelegate {
    
    func action2ButtonClicked() {
        print("-----> NextViewDelegate action2ButtonClicked)")
    }
}
