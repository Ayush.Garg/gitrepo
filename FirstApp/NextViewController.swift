//
//  NextViewController.swift
//  FirstApp
//
//  Created by cl-macmini-01 on 18/01/19.
//  Copyright © 2019 cl-macmini-01. All rights reserved.
//

import UIKit

protocol NextViewDelegate: class {
    func action2ButtonClicked()
}

class NextViewController: UIViewController {

    
    var dismissCallback: ((String) -> Void)?
    var action2Callback: ((String) -> Void)?
    weak var delegate: NextViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let leftBarItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backButtonClicked))
        self.navigationItem.leftBarButtonItem = leftBarItem
        
        let button = UIButton(type: .system)
        button.setTitle("Show Alert", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.sizeToFit()
        button.center = self.view.center
        button.addTarget(self, action: #selector(alertButtonClicked), for: .touchUpInside)
        self.view.addSubview(button)
        
    }
    
    @objc func alertButtonClicked() {
        
        let alert = UIAlertController(title: "Title", message: "Alert Message", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Dismiss", style: .default) { (action) in
            self.dismissCallback?("Dismiss")

            alert.dismiss(animated: true, completion: {
            })
            
        }
        alert.addAction(action1)

         let action2 = UIAlertAction(title: "Action 2", style: .default) { (action1) in
           self.delegate?.action2ButtonClicked()

            alert.addAction(action1)
            
        }
        
        alert.addAction(action2)
        self.present(alert, animated: true) {
        }
        
    }
    
    
    @objc func backButtonClicked() {
        self.navigationController?.popViewController(animated: true)
        
     //   self.navigationController?.popToViewController(<#T##viewController: UIViewController##UIViewController#>, animated: true)
        
       // self.navigationController?.popToRootViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
