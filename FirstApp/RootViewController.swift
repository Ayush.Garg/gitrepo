//
//  RootViewController.swift
//  FirstApp
//
//  Created by cl-macmini-01 on 17/01/19.
//  Copyright © 2019 cl-macmini-01. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        
        print("----->>>>>> buttonClicked ")
        
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
            secondViewController.secondViewTitle = "First View Message"
            
            let navController = UINavigationController(rootViewController: secondViewController)

            self.present(navController, animated: true) {
            }

        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func testFunc() {
        
    }
    
    var funcRef = testFunc
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.red
        
//        let tempView = UIView()
//        tempView.backgroundColor = UIColor.green
//        tempView.frame = CGRect(x: 0.0, y: 0.0, width: 200, height: 200)
//        self.view.addSubview(tempView)
        
        // Do any additional setup after loading the view.
    }
   
}
